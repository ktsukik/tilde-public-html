'use strict';

// packages
var gulp = require('gulp')
var del = require('del')
var sass = require('gulp-sass')
var autoprefixer = require('gulp-autoprefixer')
var cleancss = require('gulp-clean-css')
var nunjucks = require('gulp-nunjucks-render')

// project vars
const build = {dir: './build'}
const src = {
    sass: {files: './src/sass/**/*.scss'},
    html: {files: './src/**/*.html'},
    templates: {files: './src/pages/templates/*.+(html|njk)'},
    assets: {files: './src/assets/**'}
}

// build sass
gulp.task('sass', function() {
    return gulp.src(src.sass.files)
        .pipe(sass().on('error', sass.logError))
  	    .pipe(autoprefixer())
  	    .pipe(cleancss({format: 'beautify'}))
	    .pipe(gulp.dest(build.dir + '/css'))
})

// watch sass
gulp.task('sass:watch', function() {
	gulp.watch(src.sass.files, ['sass'])
})

// copy html
gulp.task('html:copy', function() {
	return gulp.src(src.html.files)
        .pipe(gulp.dest(build.dir))
})

// nunjucks
gulp.task('nunjucks:build', function() {
    return gulp.src('src/pages/*.njk')
        .pipe(nunjucks({
            path: ['src/pages']
        }))
        .pipe(gulp.dest(build.dir))
})

// nunjucks watch
gulp.task('nunjucks:watch', function() {
    gulp.watch(src.templates.files, ['nunjucks:build'])
})

// copy assets
gulp.task('asset:copy', function() {
	return gulp.src(src.assets.files)
        .pipe(gulp.dest(build.dir + '/assets'))
})

// watch html
gulp.task('html:watch', function() {
  gulp.watch(src.html.files, ['html:copy'])
})

// clean
gulp.task('clean', function() {
	return del([build.dir])
})

// build
gulp.task('build', ['sass', 'html:copy', 'nunjucks:build', 'asset:copy'])

// watch
gulp.task('watch', ['sass:watch', 'html:watch', 'nunjucks:watch'])

// default task
gulp.task('default', ['build'])
